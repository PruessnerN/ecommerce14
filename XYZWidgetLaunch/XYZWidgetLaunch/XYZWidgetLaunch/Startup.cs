﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(XYZWidgetLaunch.Startup))]
namespace XYZWidgetLaunch
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
